# TODO APP

1 Passo: clone o repositório

```bash
git clone https://gitlab.com/danielmouraa/frontend-exam.git
```

2 Passo: instale as dependências (node v11.8.0)

```bash
npm install
```

3 Passo: Simule a API REST utilizando o [json-server](https://github.com/typicode/json-server) através do arquivo database.json como entrada.

```bash
json-server --watch database.json
```

4 Passo: execute a seguinte tarefa para executar o projeto

```bash
npm run start:dev
```
